import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

public class ReportService {
    public static void main(String[] args) {

        ApiContextInitializer.init();

        TelegramLongPollingBot bot = new Bot();

/*        String proxyHost = "95.67.11.50";
        int proxyPort = 8081;
        int timeout = 75 * 1000;

        RequestConfig requestConfig = RequestConfig.custom()
                        .setProxy(new HttpHost(proxyHost, proxyPort))
                        .setSocketTimeout(timeout)
                        .setConnectionRequestTimeout(timeout)
                        .setConnectTimeout(timeout)
                        .build();

        bot.getOptions().setRequestConfig(requestConfig);*/

        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        try {
            telegramBotsApi.registerBot(bot);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
